---
layout: about
title: -EMAINLINE – About
---
## What is this?
A place where I document my attempts at porting the mainline Linux kernel to various mobile devices, from writing device trees to writing device drivers.

## Why is it called `-EMAINLINE`?
```
[   0.119556] simple-blog 5f24b080.blog: Failed to find a better name: -EMAINLINE
[   0.120357] simple-blog: probe of 5f24b080.blog failed with error -EMAINLINE
```
Jokes aside, minus error codes such as `-EINVAL`, `-ETIMEOUT` and `-EPROBE_DEFER` are something dealt with regularly when working with Linux, and from there comes the name of this blog: `-EMAINLINE`.

## What do you work on?
Devices and SoCs I work on:

### Xiaomi Mi Note 2
#### Qualcomm Snapdragon 821 (MSM8996 Pro)
I work on this device the most, thanks to the good initial mainline support for the MSM8996 SoC.

<img class="post_image" src="{{ site.url }}/assets/images/device/xiaomi-scorpio.jpg" width="400px">

- [Device postmarketOS wiki page](https://wiki.postmarketos.org/wiki/Xiaomi_Mi_Note_2_(xiaomi-scorpio))
- [SoC postmarketOS wiki page](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_820/821_(MSM8996))
- [Staging close-to-mainline kernel fork](https://gitlab.com/msm8996-mainline/linux-msm8996)

### Samsung Galaxy Grand Prime+
#### MediaTek MT6737T
This device has a broken ARM Trusted Firmware implementation that makes it impossible to boot a 64-bit OS despite having ARMv8 CPUs. It also lacks PSCI, so booting the remaining 3 CPUs is not possible without a special driver. That is perhaps made clear by the single Tux showing up:

<img class="post_image" src="{{ site.url }}/assets/images/device/samsung-grandpplte.jpg" width="400px">

- [Device postmarketOS wiki page](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Grand_Prime_Plus_(samsung-grandpplte))
- [SoC postmarketOS wiki page](https://wiki.postmarketos.org/wiki/MediaTek_MT6735)

### Lenovo IdeaTab A3000
#### MediaTek MT6589
This is an old SoC with 4x ARM Cortex-A7 CPUs and a PowerVR SGX544 GPU, which unfortunately has no open source driver.

<img class="post_image" src="{{ site.url }}/assets/images/device/lenovo-a3000.jpg" width="400px">

- [Device postmarketOS wiki page](https://wiki.postmarketos.org/wiki/Lenovo_IdeaTab_A3000_(lenovo-a3000))
- [SoC postmarketOS wiki page](https://wiki.postmarketos.org/wiki/MediaTek_MT6589)
- [Staging close-to-mainline kernel fork](https://gitlab.com/mt6589-mainline/linux)
